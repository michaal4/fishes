/**
 * @file gameScreen.h
 * @brief 
 * This file manages the actual screen of the game. 
 */

#include "Player.h"
#include "FishManager.h"
#ifndef FISHES_GAMESCREEN_H
#define FISHES_GAMESCREEN_H

/**
 * @brief
 * Draws the vicory screen on the display.
 * 
 * @param parlcd_mem_base
 * The base of the lcd display memory
*/
void show_win_screen(unsigned char *parlcd_mem_base);

/**
 * @brief
 * Draws the loss screen on the display.
 * 
 * @param parlcd_mem_base
 * The base of the lcd display memory
*/
void show_lose_screen(unsigned char *parlcd_mem_base);

/**
 * @brief
 * Shows the current score of the player on the led line.
 * 
 * @param mem_base
 * The base of the peripherals memory
 * @param player
 * The player pointer
*/
void show_led_line(unsigned char *mem_base, Player *player);

/**
 * @brief
 * Makes a RGB led flicker if the player is near any other fish.
 * 
 * @param mem_base
 * The base of the peripherals memory
 * @param player
 * The player pointer
 * @param manager
 * The fish manager class
*/
void show_danger(unsigned char *mem_base, Player *player, FishManager manager);

/**
 * @brief
 * Starts the screen with the game.
 * 
 * @param parlcd_mem_base
 * The base of the lcd display memory
 * @param mem_base
 * The base of the peripherals memory
*/
int GameScreen(unsigned char *parlcd_mem_base, unsigned char *mem_base);

#endif //FISHES_GAMESCREEN_H